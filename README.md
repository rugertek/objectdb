ObjectDB is simple python library to store python objects.
It's simply a list of dictionaries.
Each object is stored in an individual dictonary.
It's written in pure python, no third-party libraries.

See test.py file for examples.
