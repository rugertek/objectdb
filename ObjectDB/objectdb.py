from __future__ import print_function, division, unicode_literals
from uuid import uuid4
import cPickle as Pickle
import json

__author__ = 'Rodrigo Campos-Cervera & Andoni Zubizarreta, Ruger Tek.'
__version__ = 0.2


class ObjectDB(object):
    """
    Simple Database to store python objects.
    It's based on dictionaries and the json module.
    """
    def __init__(self, db, location='db/'):
        if not location.endswith('/') and location != '':
            location += '/'
        self.location = location  # Default stores the db file in a folder called db
        self.db_location = self.location + db
        try:
            self.db = self._load(db)
        except:
            self.db = dict()  # Can be change for OrderedDict which remembers the order in which items were added

    @classmethod
    def load_db(cls, db, location='db/'):  # This method returns another ObjectDB instance
        return cls(db=db, location=location)  # Plus you don't need an initial instance of the class

    def insert(self, obj=None, **kwargs):  # Insert an object with its description
        if 'oid' not in kwargs.keys():
            kwargs['oid'] = self._generate_oid()
        if obj is not None:
            doc = dict(obj=obj, **kwargs)
        else:
            doc = dict(**kwargs)
        self.db[kwargs['oid']] = doc
        self._dump()
        return kwargs['oid']

    def get(self, oid):  # Get an Object with its ObjectId
        return ObjectResult(self.db[oid])

    def find(self, **kwargs):
        for oid in self.db:
            r = 0
            for key in kwargs:
                if key in self.db[oid].keys() and kwargs[key] == self.db[oid][key]:
                    r += 1
            if r == len(kwargs):
                yield ObjectResult(self.db[oid])

    def find_one(self, **kwargs):
        for oid in self.db:
            r = 0
            for key in kwargs:
                if key in self.db[oid].keys() and kwargs[key] == self.db[oid][key]:
                    r += 1
            if r == len(kwargs):
                return ObjectResult(self.db[oid])

    def find_all(self):
        for oid in self.db:
            yield ObjectResult(self.db[oid])

    def get_instances(self, class_or_type_or_tuple):
        for oid in self.db:
            if isinstance(self.db[oid]['obj'], class_or_type_or_tuple):
                yield ObjectResult(self.db[oid])

    def update(self, data, **kwargs):
        if 'oid' in data.keys():  # Make this a possibility in the future
            del data['oid']
            print('oid cannot be changed')
        for oid in self.db:
            r = 0
            for key in kwargs:
                if key in self.db[oid].keys() and kwargs[key] == self.db[oid][key]:
                    r += 1
            if r == len(kwargs):
                self.db[oid].update(data)
        self._dump()

    def remove(self, **kwargs):
        ids = list()
        for oid in self.db:
            r = 0
            for key in kwargs:
                if key in self.db[oid].keys() and kwargs[key] == self.db[oid][key]:
                    r += 1
            if r == len(kwargs):
                ids.append(oid)
        for oid in ids:
            del self.db[oid]
        self._dump()

    def _load(self, db):  # Only meant for internal use, use __getitem__ or the load_db to change database
        with open(self.location + db, 'rb') as f:
            return Pickle.load(f)

    def _dump(self):  # This methods writes the db file to the disk, it should be called by methods that change the db
        with open(self.db_location, 'wb') as f:
            Pickle.dump(self.db, f, Pickle.HIGHEST_PROTOCOL)

    def _generate_oid(self):
        while True:
            oid = str(uuid4().hex)
            if oid not in self.db.keys():
                return oid

    def __len__(self):
        return len(self.db)

    def __repr__(self):
        return 'ObjectDB {} <{}>'.format(self.db_location, __version__)

    def __str__(self):
        return 'ObjectDB {} <{}>'.format(self.db_location, __version__)

    def __getitem__(self, db):  # Changes the db
        try:
            self.db_location = self.location + db
            self.db = self._load(db)
            return True
        except:
            return False

    def __iter__(self):
        for key in self.db:
            yield key


class ObjectResult(object):
    def __init__(self, data):
        self.__dict__ = data

    def pretty(self):
        print(json.dumps(self.__dict__, indent=3, default=str))

    @property
    def data(self):
        return self.__dict__

    @data.setter
    def data(self, data):
        self.__init__(data)

    def __getitem__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]

    def __iter__(self):
        return iter(self.__dict__.keys())

    def __repr__(self):
        return '<ObjectDB Result: {}>'.format(self.__dict__['_id'])
