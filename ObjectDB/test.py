from __future__ import print_function, unicode_literals
from objectdb import ObjectDB
from datetime import datetime


class TestObj(object):
    """
    Test Object for ObjectDB
    Use int or float as values
    """
    _data = None

    def __init__(self, **kwargs):
        self.data = kwargs

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, **kwargs):
        self._data = kwargs

    def print_data(self):
        print(self._data)

    def add_values(self):
        return sum(self._data.values())


test_obj = TestObj(a=1, b=2, c=3, d=4)
db = ObjectDB('test.db')  # loads a database or creates a new one

db.insert(test_obj, name='TestObj', created_by='John Doe', created_at=datetime.now())  # insert a new object

test_obj = db.find_one(name='TestObj')  # find one object in the database

objects = list(db.find(name='TestObj'))  # find all the objects in the database

db.update({'name': 'Test_Obj'}, name='TestObj', created_by='John Doe')  # update the object description

new_obj = TestObj(a=2, g=3, h=2)
db.update({'obj': new_obj}, name='Test_Obj')  # update the object

db.remove(name='Test_Obj')  # delete all the objects that match the description

